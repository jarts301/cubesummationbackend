package jcst.cubesum.model;

public class Operation {
	private String type;
	private Integer[] values;
	
	public Operation(String type, Integer[] values) {
		this.type = type;
		this.values = values;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public Integer[] getValues() {
		return values;
	}

	public void setValues(Integer[] values) {
		this.values = values;
	}
	
}
