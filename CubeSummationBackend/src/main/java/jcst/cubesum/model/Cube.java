package jcst.cubesum.model;

public class Cube {

	private Integer[][][] data;
	
	public Cube(int N){
		data = new Integer[N][N][N];
		zeroFill(data, N);
	}
	
	public void zeroFill(Integer[][][] data,int N){
		for (int x = 0; x<N; x++) {
			for (int y = 0; y<N; y++) {
				for (int z = 0; z<N; z++) {
					data[x][y][z]=0;
				}
			}
		}
	}

	public Integer[][][] getData() {
		return data;
	}

	public void setData(Integer[][][] data) {
		this.data = data;
	}
	
}
