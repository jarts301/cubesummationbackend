package jcst.cubesum.model;

public class Param {
	
	private int N;
	private int M;
	
	public Param(int n, int m) {
		N = n;
		M = m;
	}
	
	public int getN() {
		return N;
	}
	public void setN(int n) {
		N = n;
	}
	public int getM() {
		return M;
	}
	public void setM(int m) {
		M = m;
	}
	
}
