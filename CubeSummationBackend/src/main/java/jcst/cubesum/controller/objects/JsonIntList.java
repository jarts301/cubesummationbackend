package jcst.cubesum.controller.objects;

import java.util.List;

public class JsonIntList {
	
	private List<Integer> list;

	public JsonIntList(List<Integer> list) {
		this.list = list;
	}

	public List<Integer> getList() {
		return list;
	}

	public void setList(List<Integer> list) {
		this.list = list;
	}

}
