package jcst.cubesum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import jcst.cubesum.controller.objects.JsonIntList;
import jcst.cubesum.service.CubeService;

@RestController
public class CubeController {

	@Autowired
	CubeService cubeService;

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/performOperations")
	public @ResponseBody JsonIntList performOperations(
			@RequestParam(required = true) Integer t,
			@RequestParam(required = true) String params,
			@RequestParam(required = true) String operations) {
		
		//return cubeService.performTestCases(2, "4,5;2,4", "UPDATE,2,2,2,4;QUERY,1,1,1,3,3,3;UPDATE,1,1,1,23;QUERY,2,2,2,4,4,4;QUERY,1,1,1,3,3,3;UPDATE,2,2,2,1;QUERY,1,1,1,1,1,1;QUERY,1,1,1,2,2,2;QUERY,2,2,2,2,2,2");
		
		return new JsonIntList(cubeService.performTestCases(t, params, operations));
		
	}
	
}
