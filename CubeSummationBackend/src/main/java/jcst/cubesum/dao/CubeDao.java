package jcst.cubesum.dao;

import jcst.cubesum.model.Cube;

public interface CubeDao {
	
	public void update(Cube cube, int x, int y, int z, int W);
	
	public int query(Cube cube, int x1, int y1, int z1, int x2, int y2, int z2);
	
}
