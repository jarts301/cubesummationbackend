package jcst.cubesum.dao.impl;

import org.springframework.stereotype.Repository;

import jcst.cubesum.dao.CubeDao;
import jcst.cubesum.model.Cube;

@Repository("CubeDao")
public class CubeDaoImpl implements CubeDao {

	@Override
	public void update(Cube cube, int x, int y, int z, int W) {
		cube.getData()[x-1][y-1][z-1] = W;
	}

	@Override
	public int query(Cube cube, int x1, int y1, int z1, int x2, int y2, int z2) {
		int result=0;
		for (int x = x1-1; x<=x2-1; x++) {
			for (int y = y1-1; y<=y2-1; y++) {
				for (int z = z1-1; z<=z2-1; z++) {
					result = result+cube.getData()[x][y][z];
				}
			}
		}
		return result;
	}

}
