package jcst.cubesum.service;

import java.util.List;

import jcst.cubesum.model.Operation;

public interface CubeService {
	
	public List<Integer> performOperations(int N, List<Operation> operations);
	
	public List<Integer> performTestCases(int T, String params, String operations);
	
}
