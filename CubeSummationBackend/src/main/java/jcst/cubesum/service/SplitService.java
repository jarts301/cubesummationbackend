package jcst.cubesum.service;

import java.util.List;

import jcst.cubesum.model.Operation;
import jcst.cubesum.model.Param;

public interface SplitService {

	public List<Operation> splitData(String data);
	
	public List<Param> splitParams(String data);
	
}
