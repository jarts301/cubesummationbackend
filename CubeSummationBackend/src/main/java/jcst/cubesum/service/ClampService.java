package jcst.cubesum.service;

public interface ClampService {
	
	public int clampValue(int value, int min, int max);
	
	public Integer[] clampList(Integer[] values, int min, int max, int from, int to);
}
