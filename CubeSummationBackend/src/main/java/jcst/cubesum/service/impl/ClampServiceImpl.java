package jcst.cubesum.service.impl;

import org.springframework.stereotype.Repository;
import jcst.cubesum.service.ClampService;

@Repository("ClampService")
public class ClampServiceImpl implements ClampService {
	
	public int clampValue(int value, int min, int max){
		int result=value;
		if(value<min){
			result=min;
		}else
		if(value>max){
			result=max;
		}
		
		return result;
	}
	
	public Integer[] clampList(Integer[] values, int min, int max, int from, int to){
		Integer[] result=new Integer[values.length];
		for (int i = from; i < to; i++) {
			result[i]= clampValue(values[i], min, max);
		}
		return result;
	}
}
