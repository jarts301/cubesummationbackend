package jcst.cubesum.service.impl;

import org.springframework.stereotype.Repository;

import jcst.cubesum.service.ChangeTypeService;

@Repository("ChangeTypeService")
public class ChangeTypeServiceImpl implements ChangeTypeService {

	@Override
	public Integer[] arrayFromStrToInt(String[] values) {
		Integer[] result=new Integer[values.length];
		for (int i = 0; i < values.length; i++) {
			result[i]=Integer.parseInt(values[i]);
		}
		
		return result;
	}
	
}
