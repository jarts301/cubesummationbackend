package jcst.cubesum.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jcst.cubesum.dao.CubeDao;
import jcst.cubesum.model.Cube;
import jcst.cubesum.model.Operation;
import jcst.cubesum.model.Param;
import jcst.cubesum.service.ClampService;
import jcst.cubesum.service.CubeService;
import jcst.cubesum.service.RemoveService;
import jcst.cubesum.service.SplitService;

@Repository("CubeService")
public class CubeServiceImpl implements CubeService {

	@Autowired
	private CubeDao cubeDao;
	@Autowired
	private ClampService clamp;
	@Autowired
	private SplitService split;
	@Autowired
	private RemoveService remove;

	@Override
	public List<Integer> performOperations(int N, List<Operation> operations) {
		List<Integer> result = new ArrayList<>();
		int cN = clamp.clampValue(N, 1, 100);
		Cube cube = new Cube(cN);
		Integer[] values;
		Integer W=0;
		try {
			for (Operation operation : operations) {
				values = operation.getValues();
				switch (operation.getType()) {
				case "QUERY":
					values = clamp.clampList(values, 1, N, 0, values.length);
					result.add(cubeDao.query(cube, values[0], values[1], values[2], values[3], values[4], values[5]));
					break;
				case "UPDATE":
					W=values[3];
					values = clamp.clampList(values, 1, N, 0, values.length-1);
					cubeDao.update(cube, values[0], values[1], values[2], clamp.clampValue(W, (int)Math.pow(-10,9), (int)Math.pow(10,9) ));
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("Parameters number incorrect.");
		}

		return result;
	}

	@Override
	public List<Integer> performTestCases(int T, String paramsStr, String operationsStr) {
		List<Integer> result = new ArrayList<>();
		List<Param> params = split.splitParams(paramsStr);
		List<Operation> operations = split.splitData(operationsStr);
		Param param;
		for (int i = 0; i < T; i++) {
			param = params.get(i);
			result.addAll(performOperations(param.getN(), remove.removeSublist(operations, 0, param.getM()) ));
		}
		
		return result;
	}

}
