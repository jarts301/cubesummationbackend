package jcst.cubesum.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jcst.cubesum.model.Operation;
import jcst.cubesum.model.Param;
import jcst.cubesum.service.ChangeTypeService;
import jcst.cubesum.service.SplitService;

@Repository("SplitService")
public class SplitServiceImpl implements SplitService {
	
	@Autowired
	private ChangeTypeService changeType;
	
	public List<Operation> splitData(String data){
		List<Operation> result=new ArrayList<>();
		
		String[] operations =  data.split(";");
		String[] operationData = null;
		for (int i = 0; i < operations.length; i++) {
			operationData = operations[i].split(",");
			result.add(new Operation(operationData[0], 
					changeType.arrayFromStrToInt(
					Arrays.copyOfRange(operationData, 1, operationData.length))));
		}
		
		return result;
	}

	@Override
	public List<Param> splitParams(String data) {
		List<Param> result=new ArrayList<>();
		
		String[] params =  data.split(";");
		String[] paramsData = null;
		Integer[] paramsDataInt=null;
		for (int i = 0; i < params.length; i++) {
			paramsData = params[i].split(",");
			paramsDataInt = changeType.arrayFromStrToInt(paramsData);
			result.add(new Param(paramsDataInt[0], paramsDataInt[1]));
		}
		
		return result;
	}
	
}
