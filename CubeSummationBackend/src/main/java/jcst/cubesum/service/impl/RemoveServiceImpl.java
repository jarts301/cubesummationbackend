package jcst.cubesum.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import jcst.cubesum.model.Operation;
import jcst.cubesum.service.RemoveService;

@Repository("RemoveService")
public class RemoveServiceImpl implements RemoveService {

	@Override
	public List<Operation> removeSublist(List<Operation> operations, int from, int to) {
		List<Operation> result= new ArrayList<>();
		for (int i = from; i < to; i++) {
			result.add(operations.get(i));
		}
		operations.subList(from, to).clear();
		return result;
	}
	
}
