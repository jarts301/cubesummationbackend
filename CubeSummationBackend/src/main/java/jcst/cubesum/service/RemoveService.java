package jcst.cubesum.service;

import java.util.List;

import jcst.cubesum.model.Operation;

public interface RemoveService {
	
	public List<Operation> removeSublist(List<Operation> operations, int from, int to);
	
}
